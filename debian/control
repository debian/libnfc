Source: libnfc
Section: libs
Priority: optional
Maintainer: Nobuhiro Iwamatsu <iwamatsu@debian.org>
Uploaders: Ludovic Rousseau <rousseau@debian.org>
Build-Depends: debhelper-compat (= 13), libtool, pkg-config, libusb-dev
Standards-Version: 4.6.2
Homepage: http://www.nfc-tools.org/
Vcs-Git: https://salsa.debian.org/debian/libnfc.git
Vcs-Browser: https://salsa.debian.org/debian/libnfc
Rules-Requires-Root: no

Package: libnfc6
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Replaces: libnfc5
Breaks: libnfc5
Description: Near Field Communication (NFC) library
 libnfc is a library for Near Field Communication. It abstracts the
 low-level details of communicating with the devices away behind an
 easy-to-use high-level API.
 It supports most hardware based on the NXP PN531, PN532 or PN533
 controller chips.
 .
 This package contains the runtime library files needed to run software
 using libnfc.

Package: libnfc-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libnfc6 (= ${binary:Version}), libusb-dev
Description: Near Field Communication (NFC) library (development files)
 libnfc is a library for Near Field Communication. It abstracts the
 low-level details of communicating with the devices away behind an
 easy-to-use high-level API.
 It supports most hardware based on the NXP PN531, PN532 or PN533
 controller chips.
 .
 This package contains the header and development files needed to build
 programs and packages using libnfc.

Package: libnfc-bin
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libnfc6 (>= ${binary:Version})
Description: Near Field Communication (NFC) binaries
 libnfc is a library for Near Field Communication. It abstracts the
 low-level details of communicating with the devices away behind an
 easy-to-use high-level API.
 It supports most hardware based on the NXP PN531, PN532 or PN533
 controller chips.
 .
 This package contains some utils that come along with libnfc, for
 development or debugging purposes (including nfc-list, nfc-mfclassic,
 nfc-mfultralight, etc).

Package: libnfc-examples
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libnfc6 (>= ${binary:Version})
Description: Near Field Communication (NFC) examples
 libnfc is a library for Near Field Communication. It abstracts the
 low-level details of communicating with the devices away behind an
 easy-to-use high-level API.
 It supports most hardware based on the NXP PN531, PN532 or PN533
 controller chips.
 .
 Some examples are provided with libnfc for debugging and/or
 educational purposes (nfc-anticol, nfc-emulate, etc.).

Package: libnfc-pn53x-examples
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libnfc6 (>= ${binary:Version})
Description: Near Field Communication (NFC) examples for PN53x chips only
 libnfc is a library for Near Field Communication. It abstracts the
 low-level details of communicating with the devices away behind an
 easy-to-use high-level API.
 It supports most hardware based on the NXP PN531, PN532 or PN533
 controller chips.
 .
 Some PN53x-only examples are provided with libnfc for debugging
 and/or educational purposes (pn53x-sam, pn53x-tamashell, etc.).
